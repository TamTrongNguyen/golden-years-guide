from __init__ import db
from __init__ import app
import requests
import json
from Approved import approved_list
from models import NursingHomes, Zipcodes

def add_nm_to_db():
    with app.app_context():
        url = "https://data.cms.gov/provider-data/api/1/datastore/query/4pq5-n9py/0"

        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json'
        }

        nursing_homes_data = []
        db.session.query(NursingHomes).delete()

        for zipcode in approved_list:
            zipcode = str(zipcode)
            zipcode_o = db.session.query(Zipcodes).filter(Zipcodes.zipcode == zipcode).all()[0]
            payload = {
                "conditions": [
                    {
                        "resource": "t",
                        "property": "zip_code",
                        "value": zipcode,
                        "operator": "contains"
                    },
                ],
                "limit": 8
            }
            response = requests.post(url, headers=headers, json=payload)
            if response.status_code == 200:
                data = response.json()
                data_list = data.get("results")
                nursing_homes_data += data_list
                for results in data_list:
                    numbeds = results["number_of_certified_beds"]
                    rating = results["overall_rating"]
                    num_res = results["average_number_of_residents_per_day"]
                    hrating = results["health_inspection_rating"]
                    if(numbeds == ""):
                            numbeds = 116
                    if(rating == ""):
                            rating = 2  
                    if(num_res == ""):
                            num_res = 87.2
                    if(hrating == ""):
                            hrating = 2
                    row = NursingHomes(name = results.get("provider_name"), 
                                       address = results.get("provider_address"),
                                       city = results.get("citytown"),
                                       zipcode = results.get("zip_code"),
                                       zipcodes_id = zipcode_o.id,
                                       num_beds = numbeds, 
                                       overall_rating = rating, 
                                       council = results.get("with_a_resident_and_family_council"), 
                                       ownership_type = results.get("ownership_type"), 
                                       num_residents = num_res, 
                                       business_name = results.get("legal_business_name"), 
                                       health_rating = hrating)
                    zipcode_o.nursing_homes.append(row)
                    db.session.add(row)
                
            else:
                print(f'Error: {response.status_code} - {response.text}')

        with open("nursinghomes.json", "w") as json_file:
            json.dump(nursing_homes_data, json_file, indent = 4)

        db.session.commit()