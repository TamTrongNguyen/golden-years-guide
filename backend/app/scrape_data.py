from __init__ import db
from __init__ import app
from models import NursingHomes
from models import Geriatricians
from models import Zipcodes
from Geriatricians import add_g_to_db
from NursingHomes import add_nm_to_db
from Zipcodes import add_z_to_db

add_z_to_db()
add_nm_to_db()
add_g_to_db()

with app.app_context():
    db.session.close()


