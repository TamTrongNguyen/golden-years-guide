from __init__ import db
from __init__ import app
import requests
import json
from Approved import approved_list
from models import Zipcodes

def add_z_to_db():
    with app.app_context():
        get_one_url_prefix = "https://api.zip-codes.com/ZipCodesAPI.svc/1.0/GetZipCodeDetails/"
        get_one_url_suffix = "?key=VWRQX1SMECY48CSF5515"

        zipcodes_data = []
        db.session.query(Zipcodes).delete()
                
        for zipcode in approved_list:
            zipcode = str(zipcode)
            url2 = get_one_url_prefix + zipcode + get_one_url_suffix
            response = requests.get(url2)
            if response.status_code == 200:
                attributes = response.json()
                attributes = attributes.get("item")
                zipcodes_data.append(attributes)
                # create a zipcode model with attributes
                row = Zipcodes(zipcode = attributes.get("ZipCode"), city = attributes.get("City"), county = attributes.get("CountyName"), latitude = attributes.get("Latitude"), longitude = attributes.get("Longitude"), population = attributes.get("ZipCodePopulation"), households = attributes.get("HouseholdsPerZipcode"), prsn_per_household = attributes.get("PersonsPerHousehold"), median_age = attributes.get("MedianAge") , avg_income_household = attributes.get("IncomePerHousehold"))
                db.session.add(row)
            else:
                print(f'Error: {response.status_code} - {response.text}')
        with open("zipcodes.json", "w") as json_file:
            json.dump(zipcodes_data, json_file, indent = 4)

        db.session.commit()