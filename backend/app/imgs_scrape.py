from __init__ import db
from __init__ import app
import requests
import json
from models import NursingHomes, Geriatricians, Zipcodes
first_half = "https://serpapi.com/search.json?engine=google_images&q="
second_half = "&location=Texas&api_key=d649a71f261d0913fa399574571a256dcb5ca726cc30c15c30d1efa5d5a226f5"
with app.app_context():

    zipcodes = Zipcodes.query.all()
    for zipcode in zipcodes:
        query = zipcode.zipcode + " " + zipcode.city + " buildings"
        url = first_half + query + second_half
        data = requests.get(url).json()
        imgs = data.get("images_results")
        imgs = imgs[0].get("original")
        zipcode.img = imgs

    nursing_homes = NursingHomes.query.all()
    for nh in nursing_homes:     
        query = nh.name + " Nursing Home"
        url = first_half + query + second_half
        data = requests.get(url).json()
        imgs = data.get("images_results")
        imgs = imgs[0].get("original")
        nh.img = imgs

    geriatricians = Geriatricians.query.all()
    for i, g in enumerate(geriatricians):
        cur_name = g.first_name + " " + g.last_name
        if i < 99:
            second_half = "&location=Texas&api_key=ebe2952121507efd6d35d1c507adb67608daf4731dff4dbb21f2e1b29c91ad8d"
        else: 
            second_half = "&location=Texas&api_key=b8c123533c9c7c8cd5a37e066955f96928dd5edd282e963f1987bbf56bfb85ed"

        query = cur_name + " geriatrician Texas"
        url = first_half + query + second_half
        data = requests.get(url).json()
        imgs = data.get("images_results")
        imgs = imgs[0].get("original")
        g.img = imgs


    db.session.commit()
    db.session.close()



