from __init__ import app
from __init__ import db
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

class NursingHomes(db.Model):
    __tablename__ = 'NursingHomes'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    address = db.Column(db.String)
    city = db.Column(db.String)
    zipcode = db.Column(db.String)
    num_beds = db.Column(db.Integer)
    overall_rating = db.Column(db.Integer)
    council = db.Column(db.String)
    ownership_type = db.Column(db.String)
    num_residents = db.Column(db.Double)
    business_name = db.Column(db.String)
    health_rating = db.Column(db.Integer)
    img = db.Column(db.String)
    health_rating = db.Column(db.Integer)
    latitude = db.Column(db.Double)
    longitude = db.Column(db.Double)
    zipcode_ptr = db.relationship("Zipcodes", backref="nursing_homes")
    zipcodes_id = db.Column(db.Integer, db.ForeignKey("Zipcodes.id"))

    def __init__(self, name, address, city, zipcode, zipcodes_id, num_beds, overall_rating, council, ownership_type, num_residents, business_name, health_rating, img, latitude, longitude):
        self.name = name
        self.address = address
        self.city = city
        self.zipcode = zipcode
        self.zipcodes_id = zipcodes_id
        self.num_beds = num_beds
        self.overall_rating = overall_rating
        self.council = council
        self.ownership_type = ownership_type
        self.num_residents = num_residents
        self.business_name = business_name
        self.health_rating = health_rating
        self.img = img
        self.latitude = latitude
        self.longitude = longitude
        
class Geriatricians(db.Model):
    __tablename__ = 'Geriatricians'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    gender = db.Column(db.String)
    medical_school = db.Column(db.String)
    grad_year = db.Column(db.Integer)
    facility_name = db.Column(db.String)
    address = db.Column(db.String)
    city = db.Column(db.String)
    zipcode = db.Column(db.String)
    img = db.Column(db.String)
    latitude = db.Column(db.Double)
    longitude = db.Column(db.Double)
    zipcode_ptr = db.relationship("Zipcodes", backref="geriatricians")
    zipcodes_id = db.Column(db.Integer, db.ForeignKey("Zipcodes.id"))
    num_members = db.Column(db.Integer)
    secondary_specialization = db.Column(db.String)
    
    def __init__(self, first_name, last_name, gender, medical_school, grad_year, facility_name, address, city, zipcode, zipcodes_id, num_members, secondary_specialization, img, latitude, longitude):
        self.first_name = first_name
        self.last_name = last_name
        self.gender = gender
        self.medical_school = medical_school
        self.grad_year = grad_year
        self.facility_name = facility_name
        self.address = address
        self.city = city
        self.zipcode = zipcode
        self.zipcodes_id = zipcodes_id
        self.num_members = num_members
        self.secondary_specialization = secondary_specialization
        self.img = img
        self.latitude = latitude
        self.longitude = longitude

class Zipcodes(db.Model):
    __tablename__ = 'Zipcodes'
    id = db.Column(db.Integer, primary_key=True)
    zipcode = db.Column(db.String)
    city = db.Column(db.String)
    county = db.Column(db.String)
    latitude = db.Column(db.Double)
    longitude = db.Column(db.Double)
    population = db.Column(db.Integer)
    households = db.Column(db.Double)
    prsn_per_household = db.Column(db.Double)
    median_age = db.Column(db.Double)
    avg_income_household = db.Column(db.Double)
    img = db.Column(db.String)
    
    def __init__(self, zipcode, city, county, latitude, longitude, population, households, prsn_per_household, median_age, avg_income_household, img):
        self.zipcode = zipcode
        self.city = city
        self.county = county
        self.latitude = latitude
        self.longitude = longitude
        self.population = population
        self.households = households
        self.prsn_per_household = prsn_per_household
        self.median_age = median_age
        self.avg_income_household = avg_income_household
        self.img = img


with app.app_context():
    db.create_all()

