from __init__ import app

# Run app
def run_app():
   app.run(host='0.0.0.0', port=8080, threaded=True)

if __name__ == "__main__":
   run_app()