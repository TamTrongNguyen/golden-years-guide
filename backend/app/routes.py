from __init__ import app 
from flask import request, jsonify, Response
from models import db, NursingHomes, Geriatricians, Zipcodes
from schema import *
import sqlalchemy
from sqlalchemy import or_
from sqlalchemy.sql.expression import cast
import json
import re

def paginate_list(list, page_n, n_per_page=20):
    start_index = (page_n - 1) * n_per_page
    end_index = start_index + n_per_page
    page_sublist = list[start_index:end_index]
    return page_sublist


def return_error(msg):
    resp = Response(json.dumps({"error": msg}), mimetype="app/json")
    resp.error_code = 404
    return resp

@app.route('/')
def hello_world():
    return "Server is running!"

def search_zipcodes(terms, query):
    occurrences = {}
    for term in terms:
        clauses = [
            cast(Zipcodes.id, sqlalchemy.String).ilike('%' + term + '%'),
            Zipcodes.zipcode.ilike('%' + term + '%'),
            Zipcodes.city.ilike('%' + term + '%'),
            Zipcodes.county.ilike('%' + term + '%'),
            cast(Zipcodes.latitude, sqlalchemy.String).ilike('%' + term + '%'),
            cast(Zipcodes.longitude, sqlalchemy.String).ilike('%' + term + '%'),
            cast(Zipcodes.population, sqlalchemy.String).ilike('%' + term + '%'),
            cast(Zipcodes.households, sqlalchemy.String).ilike('%' + term + '%'),
            cast(Zipcodes.prsn_per_household, sqlalchemy.String).ilike('%' + term + '%'),
            cast(Zipcodes.median_age, sqlalchemy.String).ilike('%' + term + '%'),
            cast(Zipcodes.avg_income_household, sqlalchemy.String).ilike('%' + term + '%'),
        ]
        
        zipcodes = query.filter(or_(*clauses))
        for zipcode in zipcodes:
            if zipcode not in occurrences:
                occurrences[zipcode] = 0
            occurrences[zipcode] += 1
    return occurrences

def search_nursing_homes(terms, query):
    occurrences = {}
    for term in terms:
        clauses = [
            NursingHomes.name.ilike('%' + term + '%'),
            NursingHomes.address.ilike('%' + term + '%'),
            NursingHomes.city.ilike('%' + term + '%'),
            NursingHomes.zipcode.ilike('%' + term + '%'),
            cast(NursingHomes.num_beds, sqlalchemy.String).ilike('%' + term + '%'),
            cast(NursingHomes.overall_rating, sqlalchemy.String).ilike('%' + term + '%'),
            NursingHomes.council.ilike('%' + term + '%'),
            NursingHomes.ownership_type.ilike('%' + term + '%'),
            cast(NursingHomes.num_residents, sqlalchemy.String).ilike('%' + term + '%'),
            NursingHomes.business_name.ilike('%' + term + '%'),
            cast(NursingHomes.health_rating, sqlalchemy.String).ilike('%' + term + '%'),
            cast(NursingHomes.latitude, sqlalchemy.String).ilike('%' + term + '%'),
            cast(NursingHomes.longitude, sqlalchemy.String).ilike('%' + term + '%'),
        ]

        nursing_homes = query.filter(or_(*clauses))
        for nursing_home in nursing_homes:
            if nursing_home not in occurrences:
                occurrences[nursing_home] = 0
            occurrences[nursing_home] += 1
    return occurrences

def search_geriatricians(terms, query):
    occurrences = {}
    for term in terms:
        clauses = [
            Geriatricians.first_name.ilike('%' + term + '%'),
            Geriatricians.last_name.ilike('%' + term + '%'),
            Geriatricians.facility_name.ilike('%' + term + '%'),
            Geriatricians.city.ilike('%' + term + '%'),
            Geriatricians.gender.ilike('%' + term + '%'),
            Geriatricians.medical_school.ilike('%' + term + '%'),
            cast(Geriatricians.grad_year, sqlalchemy.String).ilike('%' + term + '%'),
            Geriatricians.address.ilike('%' + term + '%'),
            cast(Geriatricians.num_members, sqlalchemy.String).ilike('%' + term + '%'),
            Geriatricians.secondary_specialization.ilike('%' + term + '%'),
            Geriatricians.zipcode.ilike('%' + term + '%') ,
            cast(Geriatricians.latitude, sqlalchemy.String).ilike('%' + term + '%'),
            cast(Geriatricians.longitude, sqlalchemy.String).ilike('%' + term + '%'),
        ]
        
        geriatricians = query.filter(or_(*clauses))
        for geriatrician in geriatricians:
            if geriatrician not in occurrences:
                occurrences[geriatrician] = 0
            occurrences[geriatrician] += 1
    return occurrences

@app.route('/api/search', methods=["GET"])
def search_all():
    search = request.args.get("search", type=str)
    page = request.args.get("page", type=int)
    if search is None:
        return return_error("No search terms provided")
    search_terms = search.split()
    occurrences = {
        **search_zipcodes(search_terms, db.session.query(Zipcodes)),
        **search_nursing_homes(search_terms, db.session.query(NursingHomes)),
        **search_geriatricians(search_terms, db.session.query(Geriatricians)),
    }
    sorted_results = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
    count = len(sorted_results)
    if page is not None:
        sorted_results = paginate_list(sorted_results, page, 20)
    data = []
    for result in sorted_results:
        if type(result) is Zipcodes:
            data.append({'type': 'zipcode', 'data': ZipcodesSchema.dump([result])[0]})
        elif type(result) is NursingHomes:
            data.append({'type': 'nursing_home', 'data': NursingHomesSchema.dump([result])[0]})
        elif type(result) is Geriatricians:
            data.append({'type': 'geriatrician', 'data': GeriatriciansSchema.dump([result])[0]})
    response = {
        'count': count,
        'data': data
    }
    return jsonify(response)

@app.route('/api/zipcodes_count', methods=["GET"])
def get_total_zipcodes():
    count = db.session.query(Zipcodes).count()
    return jsonify(count)

@app.route('/api/nursing_homes_count', methods=["GET"])
def get_total_nursing_nomes():
    count = db.session.query(NursingHomes).count()
    return jsonify(count)

@app.route('/api/geriatricians_count', methods=["GET"])
def get_total_geriatricians():
    count = db.session.query(Geriatricians).count()
    return jsonify(count)

@app.route('/api/zipcodes', methods=["GET"])
def get_zipcodes():
    page = request.args.get("page", type=int)
    search = request.args.get("search", type=str)
    city = request.args.get("city", type=str)
    county = request.args.get("county", type=str)
    populationmin = request.args.get("populationmin", type=int)
    populationmax = request.args.get("populationmax", type=int)
    agemin = request.args.get("agemin", type=float)
    agemax = request.args.get("agemax", type=float)
    incomemin = request.args.get("incomemin", type=int)
    incomemax = request.args.get("incomemax", type=int)
    sort_type = request.args.get("sort_type")
    desc = request.args.get("desc")
    
    
    type_to_attrib = {
        "zipcode": Zipcodes.zipcode,
        "city": Zipcodes.city,
        "county": Zipcodes.county,
        "latitude": Zipcodes.latitude,
        "longitude": Zipcodes.longitude,
        "population": Zipcodes.population,
        "households": Zipcodes.households,
        "prsn_per_household": Zipcodes.prsn_per_household,
        "median_age": Zipcodes.median_age,
        "avg_income_household": Zipcodes.avg_income_household,
        
    }
    query = None
    if not sort_type:
        query = db.session.query(Zipcodes)
    else:
        if sort_type not in type_to_attrib:
            return return_error("sort_type not found")
        if desc == "1":
            query = db.session.query(Zipcodes).order_by(type_to_attrib[sort_type].desc()).filter(type_to_attrib[sort_type] != None)
        else:
            query = db.session.query(Zipcodes).order_by(type_to_attrib[sort_type]).filter(type_to_attrib[sort_type] != None)
    
    if city is not None:
        values_list = re.split(",", city)
        filters = []
        for value in values_list:
            filters.append(Zipcodes.city == value)
        if filters:
            query = query.filter(or_(*filters))
    if county is not None:
        values_list = re.split(",", county)
        filters = []
        for value in values_list:
            filters.append(Zipcodes.county == value)
        if filters:
            query = query.filter(or_(*filters))
    if populationmin is not None:
        query = query.filter(Zipcodes.population >= populationmin)
    if populationmax is not None:
        query = query.filter(Zipcodes.population <= populationmax)
    if agemin is not None:
        query = query.filter(Zipcodes.median_age >= agemin)
    if agemax is not None:
        query = query.filter(Zipcodes.median_age <= agemax)
    if incomemin is not None:
        query = query.filter(Zipcodes.avg_income_household >= incomemin)
    if incomemax is not None:
        query = query.filter(Zipcodes.avg_income_household <= incomemax)
    if search is not None:
        occurrences = search_zipcodes(search.split(), query)
        zipcodes = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
        count = len(zipcodes)
    if page is not None:
        if search is not None:
            zipcodes = paginate_list(zipcodes, page, 20)
        else:
            paginate = query.paginate(page=page, per_page=20, error_out=False)
            zipcodes = paginate.items
            count = paginate.total
    elif search is None:
        zipcodes = query.all()
        count = len(zipcodes)
    result = ZipcodesSchema.dump(zipcodes, many=True)
    response = {
        'count': count,
        'data': result
    }
    return jsonify(response)

@app.route('/api/zipcodes/<int:id>', methods=["GET"])
def get_zipcode(id):
    zipcodes = db.session.query(Zipcodes).filter_by(id=id)
    zipcodes_json = ZipcodesSchema.dump(zipcodes, many=True)
    if not zipcodes_json:
        return return_error("Zipcode ID not found")
    return zipcodes_json

@app.route('/api/nursing_homes', methods=["GET"])
def get_nursing_homes():
    page = request.args.get("page", type=int)
    search = request.args.get("search", type=str)
    city = request.args.get("city", type=str)
    council = request.args.get("council", type=str)
    bedsmin = request.args.get("bedsmin", type=int)
    bedsmax = request.args.get("bedsmax", type=int)
    ratingmin = request.args.get("ratingmin", type=int)
    ratingmax = request.args.get("ratingmax", type=int)
    healthmin = request.args.get("healthmin", type=int)
    healthmax = request.args.get("healthmax", type=int)
    sort_type = request.args.get("sort_type")
    desc = request.args.get("desc")
    type_to_attrib = {
        "name": NursingHomes.name,
        "address": NursingHomes.address,
        "city": NursingHomes.city,
        "zipcode": NursingHomes.zipcode,
        "num_beds": NursingHomes.num_beds,
        "overall_rating": NursingHomes.overall_rating,
        "health_rating": NursingHomes.health_rating,
        "latitude": NursingHomes.latitude,
        "longitude": NursingHomes.longitude,
    }
    query = None
    if not sort_type:
        query = db.session.query(NursingHomes)
    else:
        if sort_type not in type_to_attrib:
            return return_error("sort_type not found")
        if desc == "1":
            query = db.session.query(NursingHomes).order_by(type_to_attrib[sort_type].desc()).filter(type_to_attrib[sort_type] != None)
        else:
            query = db.session.query(NursingHomes).order_by(type_to_attrib[sort_type]).filter(type_to_attrib[sort_type] != None)
    if city is not None:
        values_list = re.split(",", city)
        filters = []
        for value in values_list:
            filters.append(NursingHomes.city == value)
        if filters:
            query = query.filter(or_(*filters))
    if council is not None:
        values_list = re.split(",", council)
        filters = []
        for value in values_list:
            filters.append(NursingHomes.council == value)
        if filters:
            query = query.filter(or_(*filters))
    if bedsmin is not None:
        query = query.filter(NursingHomes.num_beds >= bedsmin)
    if bedsmax is not None:
        query = query.filter(NursingHomes.num_beds <= bedsmax)
    if ratingmin is not None:
        query = query.filter(NursingHomes.overall_rating >= ratingmin)
    if ratingmax is not None:
        query = query.filter(NursingHomes.overall_rating <= ratingmax)
    if healthmin is not None:
        query = query.filter(NursingHomes.health_rating >= healthmin)
    if healthmax is not None:
        query = query.filter(NursingHomes.health_rating <= healthmax)
    if search is not None:
        occurrences = search_nursing_homes(search.split(), query)
        homes = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
        count = len(homes)
    if page is not None:
        if search is not None:
            homes = paginate_list(homes, page, 20)
        else:
            paginate = query.paginate(page=page, per_page=20, error_out=False)
            homes = paginate.items
            count = paginate.total
    elif search is None:
        homes = query.all()
        count = len(homes)
    result = NursingHomesSchema.dump(homes, many=True)
    response = {
        'count': count,
        'data': result
    }
    return jsonify(response)

@app.route('/api/nursing_homes/<int:id>', methods=["GET"])
def get_nursing_home(id):
    nursing_homes = db.session.query(NursingHomes).filter_by(id=id)
    nursing_homes_json = NursingHomesSchema.dump(nursing_homes, many=True)
    if not nursing_homes_json:
        return return_error("Nursing home ID not found")
    return nursing_homes_json

@app.route('/api/geriatricians', methods=["GET"])
def get_geriatricians():
    page = request.args.get("page", type=int)
    search = request.args.get("search", type=str)
    facility = request.args.get("facility", type=str)
    city = request.args.get("city", type=str)
    gender = request.args.get("gender", type=str)
    medical_school = request.args.get("medical_school", type=str)
    gradyearmin = request.args.get("gradyearmin", type=int)
    gradyearmax = request.args.get("gradyearmax", type=int)
    sort_type = request.args.get("sort_type")
    desc = request.args.get("desc")
    type_to_attrib = {
        "first_name": Geriatricians.first_name,
        "last_name": Geriatricians.last_name,
        "facility_name": Geriatricians.facility_name,
        "address": Geriatricians.address,
        "city": Geriatricians.city,
        "zipcode": Geriatricians.zipcode,
        "latitude": Geriatricians.latitude,
        "longitude": Geriatricians.longitude,
    }
    query = None
    if not sort_type:
        query = db.session.query(Geriatricians)
    else:
        if sort_type not in type_to_attrib:
            return return_error("sort_type not found")
        if desc == "1":
            query = db.session.query(Geriatricians).order_by(type_to_attrib[sort_type].desc()).filter(type_to_attrib[sort_type] != None)
        else:
            query = db.session.query(Geriatricians).order_by(type_to_attrib[sort_type]).filter(type_to_attrib[sort_type] != None)
    
    if facility is not None:
        values_list = re.split(">", facility)
        filters = []
        for value in values_list:
            filters.append(Geriatricians.facility_name == value)
        if filters:
            query = query.filter(or_(*filters))
    if city is not None:
        values_list = re.split(",", city)
        filters = []
        for value in values_list:
            filters.append(Geriatricians.city == value)
        if filters:
            query = query.filter(or_(*filters))
    if gender is not None:
        values_list = re.split(",", gender)
        filters = []
        for value in values_list:
            filters.append(Geriatricians.gender == value)
        if filters:
            query = query.filter(or_(*filters))
    if medical_school is not None:
        values_list = re.split(",", medical_school)
        filters = []
        for value in values_list:
            filters.append(Geriatricians.medical_school == value)
        if filters:
            query = query.filter(or_(*filters))
    if gradyearmin is not None:
        query = query.filter(Geriatricians.grad_year >= gradyearmin)
    if gradyearmax is not None:
        query = query.filter(Geriatricians.grad_year <= gradyearmax)
    if search is not None:
        occurrences = search_geriatricians(search.split(), query)
        geris = sorted(occurrences.keys(), key=lambda x: occurrences[x], reverse=True)
        count = len(geris)
    if page is not None:
        if search is not None:
            geris = paginate_list(geris, page, 20)
        else:
            paginate = query.paginate(page=page, per_page=20, error_out=False)
            geris = paginate.items
            count = paginate.total
    elif search is None:
        geris = query.all()
        count= len(geris)
    result = GeriatriciansSchema.dump(geris, many=True)
    response = {
        'count': count,
        'data': result
    }
    return jsonify(response)

@app.route('/api/geriatricians/<int:id>', methods=["GET"])
def get_geriatrician(id):
    geriatricians = db.session.query(Geriatricians).filter_by(id=id)
    geriatricians_json = GeriatriciansSchema.dump(geriatricians)
    if not geriatricians_json:
        return return_error("Geriatrician ID not found")
    return geriatricians_json

@app.route('/api/zipcode/get_geriatricians_in_zipcode/<int:id>', methods=["GET"])
def get_geriatricians_in_zipcode(id):
    zipcodes = db.session.query(Zipcodes).filter_by(id=id)
    if not zipcodes:
        return return_error("Zipcode ID not found")
    geriatricians_and_nursing_homes_json = GeriatriciansSchema.dump(zipcodes[0].geriatricians, many=True)
    if not geriatricians_and_nursing_homes_json:
        return return_error("Zipcode ID not found")
    return geriatricians_and_nursing_homes_json

@app.route('/api/zipcode/get_nursing_homes_in_zipcode/<int:id>', methods=["GET"])
def get_nursing_homes_in_zipcode(id):
    zipcodes = db.session.query(Zipcodes).filter_by(id=id)
    if not zipcodes:
        return return_error("Zipcode ID not found")
    geriatricians_and_nursing_homes_json = NursingHomesSchema.dump(zipcodes[0].nursing_homes, many=True)
    if not geriatricians_and_nursing_homes_json:
        return return_error("Zipcode ID not found")
    return geriatricians_and_nursing_homes_json

@app.route('/api/nursing_home/get_close_geriatricians/<int:id>')
def get_geriatricians_in_nursing_home_zip(id):
    nursing_home = db.session.query(NursingHomes).filter_by(id=id)
    if not nursing_home:
        return return_error("Nursing home ID not found")
    c_zip = nursing_home[0].zipcode_ptr
    geriatricians_json = GeriatriciansSchema.dump(c_zip.geriatricians, many=True)
    return geriatricians_json
    
@app.route('/api/geriatricians/get_close_nursing_homes/<int:id>')
def get_nursing_homes_in_geriatrician_zip(id):
    geriatrician = db.session.query(Geriatricians).filter_by(id=id)
    if not geriatrician:
        return return_error("Nursing home ID not found")
    c_zip = geriatrician[0].zipcode_ptr
    nursing_homes_json = NursingHomesSchema.dump(c_zip.nursing_homes, many=True)
    return nursing_homes_json

