from __init__ import db
from __init__ import app
import requests
import json
from models import NursingHomes, Geriatricians, Zipcodes

with app.app_context():
    endpoint_start = "https://maps.googleapis.com/maps/api/geocode/json?address=$"
    endpoint_end = "&key=AIzaSyAaS5GEkp_xdpGT2a_Bp_2zO0BfD0uEAoo"
        
    nursing_homes = NursingHomes.query.all()
    for nh in nursing_homes:
        key = endpoint_start + nh.address + endpoint_end
        response = requests.get(key)
        if response.status_code == 200:
            data = response.json()
            maps_data = data.get("results")
            if(len(maps_data) < 1):
                latitude = 32.7767
                longitude = -96.7970
            else :
                latitude = maps_data[0]["geometry"]["location"]["lat"]
                longitude = maps_data[0]["geometry"]["location"]["lng"]
            nh.latitude = latitude
            nh.longitude = longitude

    geriatricians = Geriatricians.query.all()
    for g in geriatricians:
        key = endpoint_start + g.address + endpoint_end
        response = requests.get(key)
        if response.status_code == 200:
            data = response.json()
            maps_data = data.get("results")
            if(len(maps_data) < 1):
                latitude = 32.7767
                longitude = -96.7970
            else :
                latitude = maps_data[0]["geometry"]["location"]["lat"]
                longitude = maps_data[0]["geometry"]["location"]["lng"]
            g.latitude = latitude
            g.longitude = longitude


    db.session.commit()
    db.session.close()