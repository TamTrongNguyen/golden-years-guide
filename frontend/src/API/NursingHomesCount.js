import { useEffect, useState } from 'react';

function NursingHomesCount() {
  const [data, setData] = useState([]);
  const url = 'https://backend.goldenyearsguide.me/api/nursing_homes_count';
  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setData(data); 
      })
      .catch((error) => {
        console.error('Error fetching the count nursing homes data:', error);
      });
  }, []);

  return data;
}

export default NursingHomesCount;