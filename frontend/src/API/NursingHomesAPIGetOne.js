import React, { useEffect, useState } from 'react';

function NursingAPIGetOne() {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch(`https://backend.goldenyearsguide.me/api/nursing_homes/${nursingHomeId}`)
      .then((response) => response.json())
      .then((data) => {
        setData(data); 
      })
      .catch((error) => {
        console.error('Error fetching geriatricians data:', error);
      });
  }, []);

  return data;
}

export default NursingAPIGetOne;