import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import HomePage from './Pages/HomePage';
import About from './Pages/About';
import ZipCodes from './Pages/ZipCodes';
import NursingHomes from './Pages/NursingHomes';
import Geriatricians from './Pages/Geriatricians';
import InstancePage from './Components/InstancePage'
import SearchPage from './Pages/SearchPage';
import Visualizations from './Pages/Visualizations';
import ProviderVisualizations from './Pages/ProviderVisualizations';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <App />
      <div>
        <Routes>
        <Route path="/" element={<HomePage/>} />
        <Route path="/About" element={<About/>} />
        <Route path="/ZipCodes" element={<ZipCodes/>} />
        <Route path="/NursingHomes" element={<NursingHomes/>} />
        <Route path="/Geriatricians" element={<Geriatricians/>} />
        <Route path="/Visualizations" element={<Visualizations/>} />
        <Route path="/ProviderVisualizations" element={<ProviderVisualizations/>} />
        <Route path="/:title" element={<InstancePage />} />
        <Route path="/SearchPage" element={<SearchPage/>} />
        </Routes>
      </div>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
