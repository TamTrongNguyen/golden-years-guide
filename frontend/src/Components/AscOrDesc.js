import React from 'react';

const AscOrDesc = ({ setDesc, setPage }) => {
  return (
      <select
          data-testid="sort"
          className="form-select form-select-lg mb-3"
          aria-label=".form-select-lg example"
          defaultValue={0}
          id="desc"
          onChange={() => {
              var value = document.getElementById("desc").value;
              setDesc(value);
              setPage(1);
          }}
          >
          <option value={0} key={0}>Ascending</option>
          <option value={1} key={1}>Descending</option>
      </select>
  );
} 
export default AscOrDesc;