import React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function SmallCard(CardProps){
    const {
        model,
        model_type,
        model_title,
        model_img,
      } = CardProps;

    const cardStyle = {
        width: '18rem',
        backgroundColor: "#d79935",
      };

      const handleLinkClick = () => {
        window.scrollTo(0, 0);
      };

    let id;
    let model_name;
    let title;
    let img;
    let attribute1_name;
    let attribute1_value;
    let attribute2_name;
    let attribute2_value;
    let attribute3_name;
    let attribute3_value;
    let attribute4_name;
    let attribute4_value;
    let attribute5_name;
    let attribute5_value;
    let attribute6_name;
    let attribute6_value;
    let attribute7_name;
    let attribute7_value;
    let attribute8_name;
    let attribute8_value;
    let attribute9_name;
    let attribute9_value;
    let zipcodes_id;
    let latitude;
    let longitude;

    if (model_type === "Nursing Homes") {
      id = model.id
      model_name = "NursingHome"
      title = model_title
      img = model_img
      attribute1_name = "City"
      attribute1_value = model.city
      attribute2_name = "Council"
      attribute2_value = model.council
      attribute3_name = "Number of Certified Beds"
      attribute3_value = model.num_beds
      attribute4_name = "Overall Rating"
      attribute4_value = model.overall_rating
      attribute5_name = "Health Inspection Rating"
      attribute5_value = model.health_rating
      attribute6_name = "Number of Residents per Day"
      attribute6_value = model.num_residents
      attribute7_name = "Legal Business Name"
      attribute7_value = model.business_name
      attribute8_name = "Address"
      attribute8_value = model.address
      attribute9_name = "Zipcode"
      attribute9_value = model.zipcode
      latitude = model.latitude
      longitude = model.longitude
      zipcodes_id = model.zipcode_ptr
    } else if (model_type === "Zipcodes") {
      id = model.id
      model_name = "Zipcode"
      title = model_title
      img = model_img
      attribute1_name = "City"
      attribute1_value = model.city
      attribute2_name = "County"
      attribute2_value = model.county
      attribute3_name = "Population"
      attribute3_value = model.population
      attribute4_name = "Median Age"
      attribute4_value = model.median_age
      attribute5_name = "Average Income per Household"
      attribute5_value = model.avg_income_household
      attribute6_name = "Latitude"
      attribute6_value = model.latitude
      attribute7_name = "Longitude"
      attribute7_value = model.longitude
      attribute8_name = "Number of Households"
      attribute8_value = model.households
      attribute9_name = "Persons per Household"
      attribute9_value = model.prsn_per_household
      latitude = model.latitude
      longitude = model.longitude
      zipcodes_id = model.id
    } else {
      id = model.id
      model_name = "Geriatrician"
      title = model_title
      img = model_img
      attribute1_name = "Facility Name"
      attribute1_value = model.facility_name
      attribute2_name = "City"
      attribute2_value = model.city
      attribute3_name = "Gender"
      attribute3_value = model.gender
      attribute4_name = "Medical School"
      attribute4_value = model.medical_school
      attribute5_name = "Graduation Year"
      attribute5_value = model.grad_year
      attribute6_name = "Address"
      attribute6_value = model.address
      attribute7_name = "Number of Members in Organization"
      attribute7_value = model.num_members
      attribute8_name = "Secondary Specialization"
      attribute8_value = model.secondary_specialization
      attribute9_name = "Zipcode"
      attribute9_value = model.zipcode
      latitude = model.latitude
      longitude = model.longitude
      zipcodes_id = model.zipcodes_id
    }

    return (
      <Link
              to={{pathname: `/${title}`}}
              state={{
                id,
                model_name,
                title, 
                img,
                attribute1_name,
                attribute1_value,
                attribute2_name,
                attribute2_value,
                attribute3_name,
                attribute3_value,
                attribute4_name,
                attribute4_value,
                attribute5_name,
                attribute5_value,
                attribute6_name,
                attribute6_value,
                attribute7_name,
                attribute7_value,
                attribute8_name,
                attribute8_value,
                attribute9_name,
                attribute9_value,
                latitude,
                longitude,
                zipcodes_id, 
             }}
             onClick={handleLinkClick} 
             >
              <Card style={cardStyle}>
                <Card.Body>
                  <Card.Img variant="top" src={model_img} style={{paddingBottom: '10%'}}/>
                  <Card.Title style={{display: 'flex', justifyContent: 'center'}}>{model_title}</Card.Title>
                </Card.Body>
              </Card>
         </Link>
      );
    }