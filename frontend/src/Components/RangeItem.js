import React from 'react';

function updateValues(newMin, newMax, setValues, setPage) {
  if (!isNaN(Number(newMin)) && !isNaN(Number(newMax))) {
    setValues([newMin, newMax]);
    setPage(1);
  }
}

export default function RangeItem(props) {
  const { values, setValues, setPage } = props;
  const isValid = (value) => {
    if (isNaN(Number(value)) || value === undefined){
      return true;
    }
  }
  

  return (
    <>
      <div style={{paddingBottom: '2%'}}>
        <label>Min Value:</label>
        <input
          type="text"
          value={values[0]}
          onChange={(e) => {
            const newMin = e.target.value;
            if (!isValid(newMin)) {
              updateValues(newMin, values[1], setValues, setPage);
            }
          }}
        />
      </div>
      <div>
        <label>Max Value:</label>
        <input
          type="text"
          value={values[1]}
          onChange={(e) => {
            const newMax = e.target.value;
            if (!isValid(newMax)) {
              updateValues(values[0], newMax, setValues, setPage);
            }
          }}
        />
      </div>
    </>
  );
}