import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownItem from './DropdownItem';
import RangeItem from './RangeItem';
import SelectDropdown from '../Components/SelectDropdown';

const facility_names = [
    "TODD RULAND DC PC",
    "MPOWER CLINICAL TX",
    "PRESTIGE DERMATOLOGY OF ALLIANCE PLLC",
    "COOK CHILDRENS PHYSICIAN NETWORK",
    "UROLOGY CLINICS OF NORTH TEXAS, PLLC",
    "CENTURY INTEGRATED PARTNERS, INC",
    "TEXAS ONCOLOGY PA",
    "MEDHEALTH",
    "VMD PRIMARY PROVIDERS NORTH TEXAS PLLC",
    "CARDIAC CATH LAB OF DALLAS LP",
    "ORTHOLONESTAR, PLLC",
    "TOPCARE MEDICAL GROUP INC",
    "INFINITE MEDICAL PC",
    "EMCARE RSN EMERGENCY PHYSICIANS PLLC",
    "RTNA, PC",
    "UNIVERSITY OF TEXAS SOUTHWESTERN MEDICAL CENTER",
    "CONCORD MEDICAL GROUP PLLC",
    "TEXAS HEALTH CARE PLLC",
    "ACCLAIM PHYSICIAN GROUP INC",
    "MINUTECLINIC DIAGNOSTIC OF TEXAS LLC",
    "JEFFREY L. MORER, OD, PA",
    "ELITE ORTHOPAEDICS OF IRVING PLLC",
    "LONESTAR HOSPITAL MEDICINE ASSOCIATES PA",
    "OPEN ARMS HEALTHCARE OF TEXAS INC",
    "LEGACY ORTHOPEDICS PLLC",
    "ASARA THERAPY PLLC",
    "CENTERWELL SENIOR PRIMARY CARE TX PA",
    "SUMMIT EMERGENCY MEDICINE PLLC",
    "INTEGRATIVE EYE PARTNERS PLLC",
    "DALLAS COUNTY HOSPITAL DISTRICT",
    "HEALTHPRO HERITAGE REHAB AND FITNESS LLC",
    "DALLAS OTOLARYNGOLOGY ASSOCIATES",
    "MED SOUTHWEST PLLC",
    "SWIFT OPTOMETRY CARE PC",
    "BE WELL PRIMARY CARE MEDICINE PLLC",
    "MEDICAL CLINIC OF NORTH TEXAS, PLLC",
    "METROPOLITAN ANESTHESIA CONSULTANTS, LLP",
    "CITRA PHYSICIAN SERVICES PLLC",
    "OLIVER STREET 5.01(A) INC",
    "VPA OF TEXAS PLLC",
    "HEALTHTEXAS PROVIDER NETWORK",
    "OAK STREET HEALTH OF TEXAS PLLC",
    "REAL TIME NEUROMONITORING ASSOCIATES OF CA PC",
    "ARCADIA ANESTHESIA PA",
    "OPENLOOP HEALTHCARE PARTNERS PC",
    "TEXAS RADIOLOGY ASSOCIATES LLP",
    "QUESTCARE INTENSIVISTS PLLC",
    "RADIOLOGY ASSOCIATES OF NORTH TEXAS P A",
    "LIGHTHOUSE ANESTHESIA PLLC",
    "APEC HOLDINGS CORP, LLC",
    "UNIVERSITY OF HOUSTON SYSTEM",
    "CLEAR SKIES ANESTHESIA PLLC",
    "TEXAS PAIN RELIEF GROUP PLLC",
    "QUESTCARE MATRIX PLLC",
    "THE CENTER FOR INTEGRATIVE COUNSELING AND PSYCHOLOGY",
    "DALLAS COUNTY MENTAL HEALTH AND MENTAL RETARDATION CENTER",
    "RONALD A STEWART DO PA",
    "ASB DOCTORS ALLIANCE INC",
    "NUTRAPHASICS, LLC",
    "ALLIED ANESTHESIA AND PAIN MANAGEMENT CONSULTANTS, L.L.P.",
    "PATHOLOGISTS BIO-MEDICAL LABORATORIES PLLC",
    "TEXAS HEALTH PHYSICIANS GROUP",
    "FOX REHABILITATION SERVICES TX LLC",
    "VISTA JV PARTNERS LLC",
    "PRIMARY HEALTH PHYSICIANS PLLC",
    "HEALTHDRIVE PODIATRY GROUP, PA",
    "PDP OF TEXAS, PLLC",
    "ENDO SEDATION LLC",
    "CAMP BOWIE ER PHYSICIANS PLLC",
    "QUANG TRONG LE DO PA",
    "SUPPORTIVE CARE LLC",
    "AGAPE FOOT CARE PA",
    "ALLEN ANESTHESIA ASSOCIATES",
    "GREATER LEWISVILLE THERAPY CENTER INC",
    "LEGACY HEALTHCARE SERVICES INC",
    "VALIANT ANESTHESIA ASSOCIATES, PLLC",
    "LUCK OPTICAL LLC",
    "MY HEALTH MY RESOURCES OF TARRANT COUNTY",
    "OPHTHALMIC PARTNERS , PA",
    "SINGLETON ASSOCIATES PA",
    "CASTLE HILLS MEDICAL GROUP",
    "IPC PAC HEALTHCARE SERVICES OF TEXAS PLLC",
    "CITY DOC URGENT CARE CENTER 2, PLLC",
    "IES CARROLLTON PLLC",
    "WELLMED MEDICAL GROUP PA",
    "DALLAS NEPHROLOGY ASSOCIATES",
    "HEARTPLACE PLLC",
    "U S ANESTHESIA PARTNERS OF TEXAS, PA",
    "ALL-CARE EYE CLINIC",
    "LEGACY COUNSELING CENTER INC",
    "DALLAS COUNTY MENTAL HEALTH AND MENTAL RETARDATION CENTER",
    "RONALD A STEWART DO PA",
    "ASB DOCTORS ALLIANCE INC",
    "NUTRAPHASICS, LLC",
    "ALLIED ANESTHESIA AND PAIN MANAGEMENT CONSULTANTS, L.L.P.",
    "PATHOLOGISTS BIO-MEDICAL LABORATORIES PLLC",
    "TEXAS HEALTH PHYSICIANS GROUP",
    "FOX REHABILITATION SERVICES TX LLC"
];

const cities = [
    "FRISCO",
    "CARROLLTON",
    "ALLEN",
    "WATAUGA",
    "PLANO",
    "GRAPEVINE",
    "RICHARDSON",
    "MCKINNEY",
    "RICHLAND HILLS",
    "HALTOM CITY",
    "WHITE SETTLEMENT",
    "DALLAS",
    "FORT WORTH"
];

const genders = [
    "M",
    "F"
];

const med_schools = [
    "UNIVERSITY OF HOUSTON  - COLLEGE OF OPTOMETRY",
    "JOHNS HOPKINS UNIVERSITY SCHOOL OF MEDICINE",
    "UNIVERSITY OF TEXAS MEDICAL BRANCH AT GALVESTON",
    "LOGAN COLLEGE OF CHIROPRACTIC",
    "OTHER",
    "UNIVERSITY OF MISSOURI, COLUMBIA SCHOOL OF MEDICINE",
    "STATE UNIVERSITY OF NEW YORK - STATE COLLEGE OPTOMETRY",
    "UNIVERSITY OF NORTH TEXAS HSC, COLLEGE OF OSTEOPATHIC MED",
    "UNIVERSITY OF TEXAS MEDICAL SCHOOL AT HOUSTON",
    "UNIVERSITY OF WISCONSIN SCHOOL OF MEDICINE",
    "NOVA SOUTHEASTERN COLLEGE OF OSTEO MEDICINE",
    "TUFTS UNIVERSITY SCHOOL OF MEDICINE",
    "UNIVERSITY OF TEXAS MEDICAL SCHOOL AT SAN ANTONIO",
    "WILLIAM M. SCHOLL COLLEGE OF PODIATRIC MEDICINE",
    "DREXEL UNIVERSITY COLLEGE OF MEDICINE",
    "UNIVERSITY OF CALIFORNIA, SAN DIEGO SCHOOL OF MEDICINE",
    "UNIVERSITY OF MIAMI, LM MILLER SCHOOL OF MEDICINE",
    "TEXAS A & M UNIVERSITY SYSTEM, HSC, COLLEGE OF MEDICINE",
    "UNIVERSITY OF KANSAS SCHOOL OF MED (KC/WICH/SAL)",
    "WARREN ALPERT MEDICAL SCHOOL OF BROWN UNIVERSITY",
    "UNIVERSITY OF OKLAHOMA COLLEGE OF MEDICINE",
    "UNIVERSITY OF TEXAS SOUTHWESTERN MEDICAL SCHOOL AT DALLAS",
    "CLEVELAND CHIROPRACTIC COLLEGE - KANSAS CITY",
    "NEW YORK COLLEGE OF PODIATRIC MEDICINE",
    "TEXAS TECH UNIVERSITY HEALTH SCIENCE CENTER SCHOOL OF MEDICINE",
    "BAYLOR COLLEGE OF MEDICINE",
    "UNIVERSITY OF MISSOURI ST LOUIS - SCHOOL OF OPTOMETRY",
    "TEMPLE UNIVERSITY SCHOOL OF PODIATRIC MEDICINE"
  ];


export default function GeriatriciansFilters(props){
    const {
        sortName,
        setSortName,
        setSort,
        sortAttribs,
        descName,
        setDescName,
        setDesc,
        descAttribs,
        selectedNames,
        setSelectedNames,
        selectedCities,
        setSelectedCities,
        selectedGenders,
        setSelectedGenders,
        selectedSchools,
        setSelectedSchools,
        yearValues,
        setYearValues,
        setPage
    } = props;
      
      return (
        <div style={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
            <SelectDropdown name={sortName} setName={setSortName} setSelected={setSort} setPage={setPage} list={sortAttribs}/>
            <SelectDropdown name={descName} setName={setDescName} setSelected={setDesc} setPage={setPage} list={descAttribs} />
            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Facility Name
                </Dropdown.Toggle>
                <Dropdown.Menu style={{ maxHeight: '200px', overflowY: 'auto' }}>
                    {facility_names.map((fn) => (
                        <DropdownItem value={fn} filters={selectedNames} setFilters={setSelectedNames} setPage={setPage}/>
                    ))}
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Cities
                </Dropdown.Toggle>
                <Dropdown.Menu style={{ maxHeight: '200px', overflowY: 'auto' }}>
                    {cities.map((city) => (
                        <DropdownItem value={city} filters={selectedCities} setFilters={setSelectedCities} setPage={setPage}/>
                    ))}
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Gender
                </Dropdown.Toggle>
                <Dropdown.Menu style={{ maxHeight: '200px', overflowY: 'auto' }}>
                    {genders.map((gender) => (
                        <DropdownItem value={gender} filters={selectedGenders} setFilters={setSelectedGenders} setPage={setPage}/>
                    ))}
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Medical School
                </Dropdown.Toggle>
                <Dropdown.Menu style={{ maxHeight: '200px', overflowY: 'auto' }}>
                    {med_schools.map((school) => (
                        <DropdownItem value={school} filters={selectedSchools} setFilters={setSelectedSchools} setPage={setPage}/>
                    ))}
                </Dropdown.Menu>
            </Dropdown>

            <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    Select Graduation Year
                </Dropdown.Toggle>
                <Dropdown.Menu style={{padding: '10%'}}>
                    <RangeItem values={yearValues} setValues={setYearValues} setPage={setPage}/>
                </Dropdown.Menu>
            </Dropdown>
        </div>
      );
}