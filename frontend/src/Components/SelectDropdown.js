import React from 'react'
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownSelectItem from './DropdownSelectItem'

const SelectDropdown = ({ name, setName, setSelected, setPage, list }) => {
  return (
    <Dropdown style={{paddingRight: '1%', paddingBottom:'1%'}}>
      <Dropdown.Toggle variant="secondary" id="dropdown-basic">
        {name}
      </Dropdown.Toggle>
      <Dropdown.Menu style={{ maxHeight: '200px', overflowY: 'auto' }}>
        {list.map((item) => (
          <DropdownSelectItem name={item[0]} setName={setName} setSelected={setSelected} attrib={item[1]} setPage={setPage}/>
        ))}
      </Dropdown.Menu>
    </Dropdown>
  );
} 
export default SelectDropdown;