import { render, screen } from '@testing-library/react';
import React from "react";
import { BrowserRouter as Router } from 'react-router-dom'; 
import InstanceCard from './InstanceCard';

describe('Instance Card Tests', () => {
    test('InstanceCard renders with sample data', () => {
        const cardProps = {
          id: '1',
          model_name: 'Sample Model',
          title: 'Sample Title',
          img: 'sample-image.jpg',
          attribute1_name: 'Attribute 1 Name',
          attribute1_value: 'Attribute 1 Value',
          attribute2_name: 'Attribute 2 Name',
          attribute2_value: 'Attribute 2 Value',
          attribute3_name: 'Attribute 3 Name',
          attribute3_value: 'Attribute 3 Value',
          attribute4_name: 'Attribute 4 Name',
          attribute4_value: 'Attribute 4 Value',
          attribute5_name: 'Attribute 5 Name',
          attribute5_value: 'Attribute 5 Value',
          attribute6_name: 'Attribute 6 Name',
          attribute6_value: 'Attribute 6 Value',
          attribute7_name: 'Attribute 7 Name',
          attribute7_value: 'Attribute 7 Value',
          attribute8_name: 'Attribute 8 Name',
          attribute8_value: 'Attribute 8 Value',
          attribute9_name: 'Attribute 9 Name',
          attribute9_value: 'Attribute 9 Value',
          zipcodes_id: '12345',
          latitude: 123.456,
          longitude: -67.890,
        };
      
        render(
          <Router>
            <InstanceCard {...cardProps} />
          </Router>
        );
      

        expect(screen.getByText('Sample Title')).toBeInTheDocument();
        expect(screen.getByText('Attribute 1 Name: Attribute 1 Value')).toBeInTheDocument();
        expect(screen.getByText('Attribute 2 Name: Attribute 2 Value')).toBeInTheDocument();
        expect(screen.getByText('Attribute 3 Name: Attribute 3 Value')).toBeInTheDocument();

      });

      test('Multiple Instance Cards', () => {
        const cardPropsArray = [
            {
                id: '1',
                model_name: 'Sample Model 1',
                title: 'Sample Title 1',
                img: 'sample-image-1.jpg',
                attribute1_name: 'Attribute 1 Name 1',
                attribute1_value: 'Attribute 1 Value 1',
                attribute2_name: 'Attribute 2 Name 1',
                attribute2_value: 'Attribute 2 Value 1',
                attribute3_name: 'Attribute 3 Name 1',
                attribute3_value: 'Attribute 3 Value 1',
            },
            {
                id: '2',
                model_name: 'Sample Model 2',
                title: 'Sample Title 2',
                img: 'sample-image-2.jpg',
                attribute1_name: 'Attribute 1 Name 2',
                attribute1_value: 'Attribute 1 Value 2',
                attribute2_name: 'Attribute 2 Name 2',
                attribute2_value: 'Attribute 2 Value 2',
                attribute3_name: 'Attribute 3 Name 2',
                attribute3_value: 'Attribute 3 Value 2',
            },
            {
                id: '3',
                model_name: 'Sample Model 3',
                title: 'Sample Title 3',
                img: 'sample-image-3.jpg',
                attribute1_name: 'Attribute 1 Name 3',
                attribute1_value: 'Attribute 1 Value 3',
                attribute2_name: 'Attribute 2 Name 3',
                attribute2_value: 'Attribute 2 Value 3',
                attribute3_name: 'Attribute 3 Name 3',
                attribute3_value: 'Attribute 3 Value 3',
            },
        ];
    
            cardPropsArray.forEach((cardProps) => {
                render(
                    <Router>
                        <InstanceCard {...cardProps} />
                    </Router>
                );
            });
    
            expect(screen.getByText('Sample Title 1')).toBeInTheDocument();
            expect(screen.getByText('Sample Title 2')).toBeInTheDocument();
            expect(screen.getByText('Sample Title 3')).toBeInTheDocument();
    
        });
    });
