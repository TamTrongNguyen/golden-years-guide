import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'; 
import DropdownItem from './DropdownItem';

describe('DropdownItem Component', () => {
  test('renders correctly', () => {
    const { container } = render(<DropdownItem />);
    expect(container).toBeInTheDocument();
  });

  test('renders checkbox label correctly', () => {
    const value = 'TestValue';
    const { getByText } = render(<DropdownItem value={value} />);
    const labelElement = getByText(value);
    expect(labelElement).toBeInTheDocument();
  });

});