import { render, screen } from '@testing-library/react';
import React from "react";
import About from './About';

describe('About Page Tests', () => {
    test('there are 5 gitlab links', () => {
        render(<About />);
        const links = screen.getAllByRole('link');
        // filter out the links that are not to gitlab
        const gitlabLinks = links.filter(link => link.href.includes('gitlab'));
        expect(gitlabLinks.length).toBe(5);
    });

    test('Technologies Used are on About Page', () => {
        render(<About />);
        const result = screen.getByText("Technologies Used");
        expect(result).toBeInTheDocument();
    });

    test('Data Sources are on About Page', () => {
        render(<About />);
        const result = screen.getByText("Data Sources");
        expect(result).toBeInTheDocument();
    });

});