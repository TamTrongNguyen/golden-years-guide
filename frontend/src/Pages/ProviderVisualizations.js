import React, { useState, useEffect } from 'react';
import PieGraph from '../Components/PieGraph';
import { ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';


export default function ProviderVisualizations(){
    const pageStyle = {
        padding: '5%',
        backgroundColor: "#91c2d4"
    };

    // different lines by type of route
    let [v1_data, setV1Data] = useState([]);
    // stops by lines and destinations
    let [v2_data, setV2Data] = useState([]);
    // scatter plot of destinations by coordinates
    let [v3_data, setV3Data] = useState("");

    useEffect(() => {
        fetch("https://backend.nocarnoproblem.net/api/lines/all")
            .then(response => response.json())
            .then(json => {
                let type_counts = {};
                for (let i = 0; i < json.length; i++){
                    if (json[i].route_type in type_counts){
                        type_counts[json[i].route_type] += 1;
                    } else {
                        type_counts[json[i].route_type] = 1;
                    }
                }
                let data = [];
                for (const [key, value] of Object.entries(type_counts)){
                    // data.push({type: key, count: value});
                    // if key is null convert to string
                    if (key === null){
                        data.push({type: "null", count: value});
                    } else {
                        data.push({type: key, count: value});
                    }
                }
                setV1Data(data);
            })

        fetch("https://backend.nocarnoproblem.net/api/destinations/all")
            .then(response => response.json())
            .then(json => {
                let data = [];
                for (let i = 0; i < json.length; i++){
                    if (json[i].reviews != null && json[i].rating != null){
                        data.push({num_reviews: json[i].reviews.length, rating: json[i].rating});
                    }
                }
                setV2Data(data);
            })
        
        fetch("https://backend.nocarnoproblem.net/api/destinations/all")
            .then(response => response.json())
            .then(json => {
                let data = [];
                for (let i = 0; i < json.length; i++){
                    data.push({latitude: json[i].location_lat, longitude: json[i].location_lon});
                }
                setV3Data(data);
            })
    }, []);
    return(
        <>
        <div style={pageStyle}>
            <br></br>
            <h1 style={{ display: 'flex', justifyContent: 'center' }}>Provider Visualizations</h1>

            <h3 style={{ display: 'flex', justifyContent: 'center', paddingTop: '5%', paddingBottom: '1%' }}>Number of Lines per Type</h3>
            <PieGraph style={{ display: 'flex', justifyContent: 'center' }} data={v1_data}/>

            <h3 style={{ display: 'flex', justifyContent: 'center', paddingTop: '5%', paddingBottom: '1%' }}>Number of Reviews and Rating per Destination</h3>
            <ResponsiveContainer width="100%" height={400}>
                <ScatterChart
                    width={500}
                    height={300}
                    margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis type="number" dataKey="rating" label={{ value: 'Rating', position: 'insideBottom', offset: 0 }}/>
                    <YAxis type="number" dataKey="num_reviews" label={{ value: 'Number of Reviews', angle: -90, position: 'inside' }}/>
                    <Tooltip />
                    <Scatter data={v2_data} fill="#7E349D" />
                </ScatterChart>
            </ResponsiveContainer>

            <h3 style={{ display: 'flex', justifyContent: 'center', paddingTop: '5%', paddingBottom: '1%' }}>Rough Visualization of Locations of Destinations on a Map</h3>
            <ResponsiveContainer width="100%" height={600}>
                <ScatterChart
                    width={500}
                    height={500}
                    margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis type="number" dataKey="latitude" domain={["dataMin", "dataMax"]}/>
                    <YAxis type="number" dataKey="longitude" domain={["dataMin", "dataMax"]}/>
                    <Tooltip />
                    <Scatter data={v3_data} fill="#00000" />
                </ScatterChart>
            </ResponsiveContainer>
        </div>
           
        </>
    )
}