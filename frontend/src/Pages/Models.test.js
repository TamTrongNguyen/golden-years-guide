import { render, screen } from '@testing-library/react';
import React from "react";
import Geriatricians from './Geriatricians';
import NursingHomes from './NursingHomes';
import Zipcodes from './ZipCodes';

describe("Geriatricians", () => {
    test('instances are being returned and rendered', () => {
        render(<Geriatricians />);
        const result = screen.getByText("Total Number of Geriatricians:");
        expect(result).toBeInTheDocument();
    });
});

describe("NursingHomes", () => {
    test('instances are being returned and rendered', () => {
        render(<NursingHomes />);
        const result = screen.getByText("Total Number of Nursing Homes:");
        expect(result).toBeInTheDocument();
    });
})

describe("Zipcodes", () => {
    test('instances are being returned and rendered', () => {
        render(<Zipcodes />);
        const result = screen.getByText("Total Number of Zipcodes:");
        expect(result).toBeInTheDocument();
    });
})