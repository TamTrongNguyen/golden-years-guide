import React, { useRef, useState, useEffect } from 'react';
function importAll(r) {
    return r.keys()?.map(r);
}
const images = importAll(require.context('../home_page_imgs', false, /\.(jpe?g)$/));


function HomePage() {
    const slidePresentationTime = 3000 
    const [currentSlide, setCurrentSlide] = useState(0) 
    var sliderInterval = useRef() 
    
    useEffect(() => {
        sliderInterval = setInterval(() => {
            setCurrentSlide((currentSlide + 1) % images.length); 
          }, slidePresentationTime);
          return () => {
            clearInterval(sliderInterval)
          }
      })
    
    return (
        <div className="Home-root">
          <div className="Home-content">
            <h1 className="Home-title">Golden Years Guide</h1>
            { <div>
              {images?.map((image, index) => (
                <img alt="homepageissues"
                  id={index}
                  key={index}
                  className={index === currentSlide ? 'Home-image Home-active' : 'Home-image'}
                  src={image}
                  style={{
                    zIndex: `-${index + 1}`
                  }}
                />
              ))}
            </div> }
          </div>
        </div>
      );
}

export default HomePage;