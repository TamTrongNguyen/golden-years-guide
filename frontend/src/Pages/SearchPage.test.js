import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import SearchPage from './SearchPage';

test('renders SearchPage without crashing', () => {
  const { container } = render(<SearchPage />);
  expect(container).toBeInTheDocument();
});

test('updates search input value correctly', () => {
  const { getByPlaceholderText } = render(<SearchPage />);
  const searchInput = getByPlaceholderText('Search website');

  fireEvent.change(searchInput, { target: { value: 'test search' } });

  expect(searchInput.value).toBe('test search');
});

