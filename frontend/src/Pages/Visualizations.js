import React, { useRef } from 'react';
import PieGraph from '../Components/PieGraph';
import BarGraph from '../Components/BarGraph';
import RadialChart from '../Components/RadialChart';

const Visualizations = () => {
  const pageStyle = {
    padding: '5%',
    backgroundColor: "#91c2d4",
  };

  const v1_data = [
    {
      type: 'Frisco',
      count: 2,
    },
    {
      type: 'Carrolton',
      count: 2,
    },
    {
      type: 'Allen',
      count: 2,
    },
    {
      type: 'Plano',
      count: 3,
    },
    {
      type: 'Haltom City',
      count: 1,
    },
    {
      type: 'Dallas',
      count: 19,
    },
    {
      type: 'Fort Worth',
      count: 15,
    },
    {
      type: 'Richardson',
      count: 2,
    },
    {
      type: 'Keller',
      count: 1,
    },
    {
      type: 'McKinney',
      count: 3,
    },
    {
      type: 'Grapevine',
      count: 1,
    },
  ];

  const v2_data = [
    {
      xvalue: 'Dallas',
      count: 31,
    },
    {
      xvalue: 'Forth Worth',
      count: 30,
    },
    {
      xvalue: 'Richardson',
      count: 6,
    },
    {
      xvalue: 'Keller',
      count: 1,
    },
    {
      xvalue: 'McKinney',
      count: 5,
    },
    {
      xvalue: 'Frisco',
      count: 3,
    },
    {
      xvalue: 'Carrolton',
      count: 5,
    },
    {
      xvalue: 'Allen',
      count: 2,
    },
    {
      xvalue: 'Watuga',
      count: 1,
    },
    {
      xvalue: 'Richland Hills',
      count: 1,
    },
    {
      xvalue: 'Plano',
      count: 6,
    },
    {
      xvalue: 'White Stlmnt',
      count: 2,
    },
    {
      xvalue: 'Grapevine',
      count: 3,
    }
  ];

  const v3_data = [
    {
      city: 'Dallas',
      count: 57,
      fill: '#8884d8',
    },
    {
      city: 'Forth Worth',
      count: 41,
      fill: '#83a6ed',
    },
    {
      city: 'Richardson',
      count: 6,
      fill: '#8dd1e1',
    },
    {
      city: 'Keller',
      count: 1,
      fill: '#82ca9d',
    },
    {
      city: 'McKinney',
      count: 9,
      fill: '#a4de6c',
    },
    {
      city: 'Frisco',
      count: 6,
      fill: '#d0ed57',
    },
    {
      city: 'Carrolton',
      count: 6,
      fill: '#ffc658',
    },
    {
      city: 'Allen',
      count: 6,
      fill: '#8884d8',
    },
    {
      city: 'Watuga',
      count: 3,
      fill: '#83a6ed',
    },
    {
      city: 'Richland Hills',
      count: 2,
      fill: '#8dd1e1',
    },
    {
      city: 'Plano',
      count: 9,
      fill: '#82ca9d',
    },
    {
      city: 'White Stlmnt',
      count: 1,
      fill: '#a4de6c',
    },
    {
      city: 'Grapevine',
      count: 3,
      fill: '#d0ed57',
    },
    {
      city: 'Haltom City',
      count: 2,
      fill: '#ffc658',
    }
  ];
  
  return (
    <div style={pageStyle}>
      <br />
      <h1 style={{ display: 'flex', justifyContent: 'center', paddingBottom: '3%' }}>Visualizations</h1>

      <h3 style={{ display: 'flex', justifyContent: 'center' }}>Number of Zipcodes in Each City</h3>
      <PieGraph style={{ display: 'flex', justifyContent: 'center' }} data={v1_data} />

      <h3 style={{ display: 'flex', justifyContent: 'center', paddingTop: '5%', paddingBottom: '1%' }}>Number of Nursing Homes per City</h3>
      <BarGraph style={{ display: 'flex', justifyContent: 'center' }} data={v2_data}/>

      <h3 style={{ display: 'flex', justifyContent: 'center', paddingTop: '5%', paddingBottom: '1%' }}>Number of Geriatricians per City</h3>
      <RadialChart style={{ display: 'flex', justifyContent: 'center' }} data={v3_data}/>

    </div>
  );
};

export default Visualizations;
