import React, { useEffect, useState } from 'react';
import InstanceCard from '../Components/InstanceCard';
import GeriatricianAPI from '../API/GeriatricianAPI';
import { Button, ButtonGroup } from 'react-bootstrap';
import GeriatriciansFilters from '../Components/GeriatriciansFilters';

export default function Geriatricians(){
    const [geriatricians_count, setCount] = useState([]);
    const total_pages = Math.ceil(geriatricians_count / 20);
    const containerStyle = {
        display: 'flex',
        justifyContent: 'center',
        paddingBottom: '5%',
        flexWrap: 'wrap',
        gap: '2%'
      };

      const pageStyle = {
        padding: '5%',
        backgroundColor: "#2a596e",
      };

    
    const [sortName, setSortName] = useState('Sort');
    const [descName, setDescName] = useState('Ascending');
    const [geriatricians_data, setData] = useState([]);
    const [curpage, setPage] = useState(1);
    const [sort, setSort] = useState('');
    const [desc, setDesc] = useState(0);
    const [selectedNames, setSelectedNames] = useState([]);
    const [selectedCities, setSelectedCities] = useState([]);
    const [selectedGenders, setSelectedGenders] = useState([]);
    const [selectedSchools, setSelectedSchools] = useState([]);
    const [yearValues, setYearValues] = useState(['', '']);
    const [searchTerm, setSearchTerm] = useState('');

    useEffect(() => {
        const fetchDataforPage = async (curpage, selectedNames, selectedCities, selectedGenders, selectedSchools, yearValues, sort, desc, search) => {
            try {
                const data = await GeriatricianAPI(curpage, selectedNames, selectedCities, selectedGenders, selectedSchools, yearValues, sort, desc, search);
                setData(data.data);
                setCount(data.count);
                
            } catch (error) {
                console.error('Error fetching geriatricians data on call:', error);
            }
          };
          fetchDataforPage(curpage, selectedNames, selectedCities, selectedGenders, selectedSchools, yearValues, sort, desc, searchTerm);
    }, [curpage, selectedNames, selectedCities, selectedGenders, selectedSchools, yearValues, sort, desc, searchTerm]);

    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
        setPage(1);
    };

    let items= []
    const buttonStyles = {
        backgroundColor: "#91c2d4",
        color: 'black',
    };
    const group = {
        display: 'block',
        textAlign: 'center',
        paddingBottom: '2%'
    }
    
    for (let number = 1; number <= total_pages; number++) {
        if (curpage == number){
            items.push(
                <Button variant="secondary" key={number} onClick={() => setPage(number)}>
                  {number}
                </Button>,
            );
        } else {
            items.push(
                <Button style={buttonStyles} key={number} onClick={() => setPage(number)}>
                  {number}
                </Button>,
            );
        } 
    }

    return(
        <div style={pageStyle}>
            <div style={{ color: 'white' }}>
                <h2>{"Total Number of Geriatricians: " + String(geriatricians_count)}</h2>
                <div style={{paddingTop: '1%', paddingBottom: '1%'}}>
                    <GeriatriciansFilters 
                        sortName={sortName}
                        setSortName={setSortName}
                        setSort={setSort}
                        sortAttribs={sortAttribs}
                        descName={descName}
                        setDescName={setDescName}
                        setDesc={setDesc}
                        descAttribs={descAttribs}
                        selectedNames={selectedNames}
                        setSelectedNames={setSelectedNames}
                        selectedCities={selectedCities}
                        setSelectedCities={setSelectedCities}
                        selectedGenders={selectedGenders}
                        setSelectedGenders={setSelectedGenders}
                        selectedSchools={selectedSchools}
                        setSelectedSchools={setSelectedSchools}
                        yearValues={yearValues}
                        setYearValues={setYearValues}
                        setPage={setPage}
                    />
                </div> 
                
                <div style={{ display: 'flex', justifyContent: 'center', paddingBottom: '2%'}}>
                    <input
                        type="text"
                        placeholder="Search Geriatricians"
                        value={searchTerm}
                        onChange={handleSearchChange}
                        style={{
                        width: '300px',
                        height: '40px',
                        fontSize: '16px',
                        textAlign: 'center',
                        }}
                    />
                </div>
                <ButtonGroup style={group}>{items}</ButtonGroup>
            </div>
            <div style={containerStyle}>
                {geriatricians_data?.map (geriatrician => (
                    <div style = {{paddingBottom: '5%'}}>
                        <InstanceCard
                        id = {geriatrician.id}
                        model_name = "Geriatrician"
                        title = {geriatrician.first_name + " " + geriatrician.last_name}
                        img= {geriatrician.img}
                        attribute1_name = "Facility Name"
                        attribute1_value = {geriatrician.facility_name}
                        attribute2_name = "City"
                        attribute2_value = {geriatrician.city}
                        attribute3_name = "Gender"
                        attribute3_value = {geriatrician.gender}
                        attribute4_name = "Medical School"
                        attribute4_value = {geriatrician.medical_school}
                        attribute5_name = "Graduation Year"
                        attribute5_value = {geriatrician.grad_year}
                        attribute6_name = "Address"
                        attribute6_value = {geriatrician.address}
                        attribute7_name = "Number of Members in Organization"
                        attribute7_value = {geriatrician.num_members}
                        attribute8_name = "Secondary Specialization"
                        attribute8_value = {geriatrician.secondary_specialization}
                        attribute9_name = "Zipcode"
                        attribute9_value = {geriatrician.zipcode}
                        latitude={geriatrician.latitude}
                        longitude={geriatrician.longitude} 
                        zipcodes_id = {geriatrician.zipcodes_id}
                        search = {searchTerm}
                     />
                    </div>     
                ))}
            </div>
        </div>
    );
}

const sortAttribs = [
    ["Sort", undefined],
    ["First Name", "first_name"],
    ["Last Name", "last_name"],
    ["City", "city"],
    ["Facility Name", "facility_name"],
];

const descAttribs = [
    ["Ascending", "0"],
    ["Descending", "1"],
];

const facility_names = [
    "TODD RULAND DC PC",
    "MPOWER CLINICAL TX",
    "PRESTIGE DERMATOLOGY OF ALLIANCE PLLC",
    "COOK CHILDRENS PHYSICIAN NETWORK",
    "UROLOGY CLINICS OF NORTH TEXAS, PLLC",
    "CENTURY INTEGRATED PARTNERS, INC",
    "TEXAS ONCOLOGY PA",
    "MEDHEALTH",
    "VMD PRIMARY PROVIDERS NORTH TEXAS PLLC",
    "CARDIAC CATH LAB OF DALLAS LP",
    "ORTHOLONESTAR, PLLC",
    "TOPCARE MEDICAL GROUP INC",
    "INFINITE MEDICAL PC",
    "EMCARE RSN EMERGENCY PHYSICIANS PLLC",
    "RTNA, PC",
    "UNIVERSITY OF TEXAS SOUTHWESTERN MEDICAL CENTER",
    "CONCORD MEDICAL GROUP PLLC",
    "TEXAS HEALTH CARE PLLC",
    "ACCLAIM PHYSICIAN GROUP INC",
    "MINUTECLINIC DIAGNOSTIC OF TEXAS LLC",
    "JEFFREY L. MORER, OD, PA",
    "ELITE ORTHOPAEDICS OF IRVING PLLC",
    "LONESTAR HOSPITAL MEDICINE ASSOCIATES PA",
    "OPEN ARMS HEALTHCARE OF TEXAS INC",
    "LEGACY ORTHOPEDICS PLLC",
    "ASARA THERAPY PLLC",
    "CENTERWELL SENIOR PRIMARY CARE TX PA",
    "SUMMIT EMERGENCY MEDICINE PLLC",
    "INTEGRATIVE EYE PARTNERS PLLC",
    "DALLAS COUNTY HOSPITAL DISTRICT",
    "HEALTHPRO HERITAGE REHAB AND FITNESS LLC",
    "DALLAS OTOLARYNGOLOGY ASSOCIATES",
    "MED SOUTHWEST PLLC",
    "SWIFT OPTOMETRY CARE PC",
    "BE WELL PRIMARY CARE MEDICINE PLLC",
    "MEDICAL CLINIC OF NORTH TEXAS, PLLC",
    "METROPOLITAN ANESTHESIA CONSULTANTS, LLP",
    "CITRA PHYSICIAN SERVICES PLLC",
    "OLIVER STREET 5.01(A) INC",
    "VPA OF TEXAS PLLC",
    "HEALTHTEXAS PROVIDER NETWORK",
    "OAK STREET HEALTH OF TEXAS PLLC",
    "REAL TIME NEUROMONITORING ASSOCIATES OF CA PC",
    "ARCADIA ANESTHESIA PA",
    "OPENLOOP HEALTHCARE PARTNERS PC",
    "TEXAS RADIOLOGY ASSOCIATES LLP",
    "QUESTCARE INTENSIVISTS PLLC",
    "RADIOLOGY ASSOCIATES OF NORTH TEXAS P A",
    "LIGHTHOUSE ANESTHESIA PLLC",
    "APEC HOLDINGS CORP, LLC",
    "UNIVERSITY OF HOUSTON SYSTEM",
    "CLEAR SKIES ANESTHESIA PLLC",
    "TEXAS PAIN RELIEF GROUP PLLC",
    "QUESTCARE MATRIX PLLC",
    "THE CENTER FOR INTEGRATIVE COUNSELING AND PSYCHOLOGY",
    "DALLAS COUNTY MENTAL HEALTH AND MENTAL RETARDATION CENTER",
    "RONALD A STEWART DO PA",
    "ASB DOCTORS ALLIANCE INC",
    "NUTRAPHASICS, LLC",
    "ALLIED ANESTHESIA AND PAIN MANAGEMENT CONSULTANTS, L.L.P.",
    "PATHOLOGISTS BIO-MEDICAL LABORATORIES PLLC",
    "TEXAS HEALTH PHYSICIANS GROUP",
    "FOX REHABILITATION SERVICES TX LLC",
    "VISTA JV PARTNERS LLC",
    "PRIMARY HEALTH PHYSICIANS PLLC",
    "HEALTHDRIVE PODIATRY GROUP, PA",
    "PDP OF TEXAS, PLLC",
    "ENDO SEDATION LLC",
    "CAMP BOWIE ER PHYSICIANS PLLC",
    "QUANG TRONG LE DO PA",
    "SUPPORTIVE CARE LLC",
    "AGAPE FOOT CARE PA",
    "ALLEN ANESTHESIA ASSOCIATES",
    "GREATER LEWISVILLE THERAPY CENTER INC",
    "LEGACY HEALTHCARE SERVICES INC",
    "VALIANT ANESTHESIA ASSOCIATES, PLLC",
    "LUCK OPTICAL LLC",
    "MY HEALTH MY RESOURCES OF TARRANT COUNTY",
    "OPHTHALMIC PARTNERS , PA",
    "SINGLETON ASSOCIATES PA",
    "CASTLE HILLS MEDICAL GROUP",
    "IPC PAC HEALTHCARE SERVICES OF TEXAS PLLC",
    "CITY DOC URGENT CARE CENTER 2, PLLC",
    "IES CARROLLTON PLLC",
    "WELLMED MEDICAL GROUP PA",
    "DALLAS NEPHROLOGY ASSOCIATES",
    "HEARTPLACE PLLC",
    "U S ANESTHESIA PARTNERS OF TEXAS, PA",
    "ALL-CARE EYE CLINIC",
    "LEGACY COUNSELING CENTER INC",
    "DALLAS COUNTY MENTAL HEALTH AND MENTAL RETARDATION CENTER",
    "RONALD A STEWART DO PA",
    "ASB DOCTORS ALLIANCE INC",
    "NUTRAPHASICS, LLC",
    "ALLIED ANESTHESIA AND PAIN MANAGEMENT CONSULTANTS, L.L.P.",
    "PATHOLOGISTS BIO-MEDICAL LABORATORIES PLLC",
    "TEXAS HEALTH PHYSICIANS GROUP",
    "FOX REHABILITATION SERVICES TX LLC"
];

const cities = [
    "FRISCO",
    "CARROLLTON",
    "CAROLLTON",
    "ALLEN",
    "WATAUGA",
    "PLANO",
    "GRAPEVINE",
    "FT WORTH",
    "RICHARDSON",
    "MCKINNEY",
    "RICHLAND HILLS",
    "HALTOM CITY",
    "WHITE SETTLEMENT",
    "DALLAS",
    "FORT WORTH"
];

const genders = [
    "M",
    "F"
];

const med_schools = [
    "UNIVERSITY OF HOUSTON  - COLLEGE OF OPTOMETRY",
    "JOHNS HOPKINS UNIVERSITY SCHOOL OF MEDICINE",
    "UNIVERSITY OF TEXAS MEDICAL BRANCH AT GALVESTON",
    "LOGAN COLLEGE OF CHIROPRACTIC",
    "OTHER",
    "UNIVERSITY OF MISSOURI, COLUMBIA SCHOOL OF MEDICINE",
    "STATE UNIVERSITY OF NEW YORK - STATE COLLEGE OPTOMETRY",
    "UNIVERSITY OF NORTH TEXAS HSC, COLLEGE OF OSTEOPATHIC MED",
    "UNIVERSITY OF TEXAS MEDICAL SCHOOL AT HOUSTON",
    "UNIVERSITY OF WISCONSIN SCHOOL OF MEDICINE",
    "NOVA SOUTHEASTERN COLLEGE OF OSTEO MEDICINE",
    "TUFTS UNIVERSITY SCHOOL OF MEDICINE",
    "UNIVERSITY OF TEXAS MEDICAL SCHOOL AT SAN ANTONIO",
    "WILLIAM M. SCHOLL COLLEGE OF PODIATRIC MEDICINE",
    "DREXEL UNIVERSITY COLLEGE OF MEDICINE",
    "UNIVERSITY OF CALIFORNIA, SAN DIEGO SCHOOL OF MEDICINE",
    "UNIVERSITY OF MIAMI, LM MILLER SCHOOL OF MEDICINE",
    "TEXAS A & M UNIVERSITY SYSTEM, HSC, COLLEGE OF MEDICINE",
    "UNIVERSITY OF KANSAS SCHOOL OF MED (KC/WICH/SAL)",
    "WARREN ALPERT MEDICAL SCHOOL OF BROWN UNIVERSITY",
    "UNIVERSITY OF OKLAHOMA COLLEGE OF MEDICINE",
    "UNIVERSITY OF TEXAS SOUTHWESTERN MEDICAL SCHOOL AT DALLAS",
    "CLEVELAND CHIROPRACTIC COLLEGE - KANSAS CITY",
    "NEW YORK COLLEGE OF PODIATRIC MEDICINE",
    "TEXAS TECH UNIVERSITY HEALTH SCIENCE CENTER SCHOOL OF MEDICINE",
    "BAYLOR COLLEGE OF MEDICINE",
    "UNIVERSITY OF MISSOURI ST LOUIS - SCHOOL OF OPTOMETRY",
    "TEMPLE UNIVERSITY SCHOOL OF PODIATRIC MEDICINE"
  ];
