import React from 'react';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom'; 
import NavBar from './NavBar';

describe('NavBar Tests', () => {
    test('Navbar renders correctly', () => {
        const { getByText } = render(
          <MemoryRouter>
            <NavBar />
          </MemoryRouter>
        );
      
        expect(getByText('Golden Years Guide')).toBeInTheDocument();
        expect(getByText('Home')).toBeInTheDocument();
        expect(getByText('About')).toBeInTheDocument();
        expect(getByText('ZipCodes')).toBeInTheDocument();
        expect(getByText('Nursing Homes')).toBeInTheDocument();
        expect(getByText('Geriatricians')).toBeInTheDocument();
      });

      test('Navigation links should point to the correct paths', () => {
        const { getByText } = render(
          <MemoryRouter>
            <NavBar />
          </MemoryRouter>
        );

        const homeLink = getByText('Home');
        const aboutLink = getByText('About');
        const zipCodesLink = getByText('ZipCodes');
        const nursingHomesLink = getByText('Nursing Homes');
        const geriatriciansLink = getByText('Geriatricians');

        expect(homeLink.getAttribute('href')).toBe('/');
        expect(aboutLink.getAttribute('href')).toBe('/About');
        expect(zipCodesLink.getAttribute('href')).toBe('/ZipCodes');
        expect(nursingHomesLink.getAttribute('href')).toBe('/NursingHomes');
        expect(geriatriciansLink.getAttribute('href')).toBe('/Geriatricians');
    });
})
