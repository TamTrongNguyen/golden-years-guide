import { React, useState, useEffect } from 'react';
import { Card } from 'react-bootstrap';

import saarthak from '../team_member_pics/saarthak.jpg'
import samika from '../team_member_pics/samika.jpg'
import meghana from '../team_member_pics/meghana.jpg'
import tam from '../team_member_pics/tam.png'
import caleb from '../team_member_pics/caleb.jpg'

function AboutCard(CardProps){
    const {
        name,
        img,
        gitlab_id,
        bio,
        responsibilities,
        number_of_unit_tests
    } = CardProps

    const cardStyle = {
       width: '16rem',
       backgroundColor: "#d79935",
      };

    const gitlab_url = "https://gitlab.com/";
    const gitlab_api_url = gitlab_url + "api/v4/";
    const gitlab_project_api_url = gitlab_api_url + "projects/samikaiyer1%2Fgroup_23";

    let [number_of_commits, setNumberOfCommits] = useState("");
    let [number_of_issues, setNumberOfIssues] = useState("");

    useEffect(() => {
        // set number of commits
        fetch(gitlab_project_api_url + "/repository/contributors")
            .then(response => response.json())
            .then(json => {
                let commits = 0;
                for (let i = 0; i < json.length; i++){
                    if (json[i].name === gitlab_id || json[i].name === name){
                        commits = json[i].commits;
                    }
                }
                setNumberOfCommits(commits);
            })
        
        // set number of issues
        fetch(gitlab_project_api_url + "/issues_statistics?assignee_username=" + gitlab_id)
            .then(response => response.json())
            .then(json => setNumberOfIssues(json.statistics.counts.all))
        
    }, [gitlab_api_url, gitlab_id, gitlab_project_api_url, name]);

    return (
       <Card style={cardStyle}>
                <Card.Img variant="top" src= {img} />
                    <Card.Body style={{ textAlign: 'center' }}>
                        <Card.Title>{name}</Card.Title>
                        <Card.Text>{"Gitlab: "}<a href={gitlab_url + gitlab_id}>{gitlab_id}</a></Card.Text>
                        <Card.Text>{bio}</Card.Text>
                        <Card.Text>{responsibilities}</Card.Text>
                        <Card.Text>{"Commits: " + number_of_commits}</Card.Text>
                        <Card.Text>{"Issues: " + number_of_issues}</Card.Text>
                        <Card.Text>{"Unit Tests: " + number_of_unit_tests}</Card.Text>
                    </Card.Body>
            </Card>
    );
}

export default function About(){
    const containerStyle = {
        display: 'flex',
        justifyContent: 'center',
        paddingBottom: '2%',
        flexWrap: 'wrap'
    };

    const pageStyle = {
        padding: '5%',
        backgroundColor: "#2a596e",
    };

    const textStyle = {
        color: "white",
    };

    return(
        <div style={pageStyle}>
            <div style={textStyle}>
                <h1>About Golden Years Guide</h1>
                <header id="about">The elderly population in DFW Metroplex constitutes a significant and growing underserved community, facing a range of challenges that require specialized care and support. With the demographic shift towards an aging population, the demand for adequate healthcare services has surged, placing immense strain on resources. Many seniors struggle to access the right facilities, such as nursing homes and rehabilitation centers, due to a lack of information or guidance. In this context, a comprehensive guide to nursing homes and doctors is immensely helpful. It not only empowers elderly individuals and their families with vital information on facility options, services offered, and quality ratings but also aids in making informed decisions about the best care options available. Such a resource plays a crucial role in ensuring that our elderly citizens receive the care and attention they rightfully deserve, enhancing their quality of life and well-being in their golden years.
                </header>
                <br></br>
                <a href="https://documenter.getpostman.com/view/30032226/2s9YXib37u" target="_blank" rel="noopener noreferrer">
                    <h1>Postman API</h1>
                </a>
                <a href="https://gitlab.com/samikaiyer1/group_23" target="_blank" rel="noopener noreferrer">
                    <h1>GitLab Repo</h1>
                </a>
                <br></br>
                <h1>The Team Members</h1>
                <div style={containerStyle}>
                    <AboutCard
                        name = "Samika Iyer"
                        img = {samika}
                        gitlab_id = "samikaiyer1"
                        bio = "I'm a third-year Computer Science student at UT Austin, and I am pursuing a minor in Critical Disability Studies. I enjoy working as a caregiver, trying new restaurants, and hanging out with my friends."
                        responsibilities = "Full Stack"
                        number_of_unit_tests = "13"
                    ></AboutCard>
                    <AboutCard
                        name = "Meghana Inaganti"
                        img = {meghana}
                        gitlab_id = "meghanainaganti"
                        bio = "I am a third year Computer Science student at UT Austin and am also minoring in business. I enjoy reading, dancing, and watching movies."
                        responsibilities = "Full Stack"
                        number_of_unit_tests = "12"
                    ></AboutCard>
                    <AboutCard
                        name = "Saarthak Mohan"
                        img = {saarthak}
                        gitlab_id = "SaarthakMohan"
                        bio = "I am a senior Computer Science student at UT Austin. I enjoy playing video games and participating in martial arts, especially Brazilian Jiu Jitsu."
                        responsibilities = "Backend"
                        number_of_unit_tests = "6"
                    ></AboutCard>
                    <AboutCard
                        name = "Tam Nguyen"
                        img = {tam}
                        gitlab_id = "TamTrongNguyen"
                        bio = "I'm a senior at UT Austin majoring in Computer Science. I like to play games, stay active, read, and watch movies."
                        responsibilities = "Frontend"
                        number_of_unit_tests = "10"
                    ></AboutCard>
                    <AboutCard
                        name = "Caleb Hu"
                        img = {caleb}
                        gitlab_id = "bleh05"
                        bio = "I am a junior majoring in CS + math. I play games, run sometimes, and do competitive programming."
                        responsibilities = "Backend"
                        number_of_unit_tests = "0"
                    ></AboutCard>
            </div>
            <h1>Interesting Result</h1>
                <header id="interesting result">
                    In our project centered around nursing homes and geriatricians in the DFW Metroplex, we embarked on a data-gathering journey 
                    from multiple sources to create a comprehensive resource. What truly captivated our attention was the profound depth of support available for the elderly 
                    within the region. The integration of data from these disparate sources unveiled a remarkably interconnected healthcare network, demonstrating the collaborative efforts of various providers and institutions. 
                    This integration not only empowered us to construct an invaluable guide but also shed light on the remarkable synergy within the healthcare community, 
                    emphasizing how these sources intricately relate to one another, ultimately benefiting our senior citizens and their families.
                </header>
                <br></br>
                <h1>Technologies Used</h1>
                <header id="technologies">
                    * React <br></br>
                    * React Bootstrap <br></br>
                    * AWS Amplify <br></br>
                    * AWS RDS <br></br>
                    * AWS EC2 <br></br>
                    * Postman <br></br>
                    * PostgreSQL <br></br>
                    * Flask <br></br>
                    * SQL Alchemy <br></br>
                    * SQL Workbench <br></br>
                    * Jest <br></br>
                    * Selenium <br></br>
                    * D3 <br/>
                </header>
                <br></br>
                <h1>Data Sources</h1>
                <header id="data_sources">
                * <a href=" https://www.texashealthmaps.com/lfex"> https://www.texashealthmaps.com/lfex</a><br></br>
                * <a href=" https://health.usnews.com/doctors/geriatricians"> https://health.usnews.com/doctors/geriatricians</a><br></br>
                * <a href=" https://www.hhs.texas.gov/providers/long-term-care-providers/nursing-facilities-nf"> https://www.hhs.texas.gov/providers/long-term-care-providers/nursing-facilities-nf</a><br></br>
                * <a href=" https://data.cms.gov/provider-data/dataset/4pq5-n9py"> https://data.cms.gov/provider-data/dataset/4pq5-n9py</a><br></br>
                * <a href=" https://data.cms.gov/provider-data/dataset/mj5m-pzi6"> https://data.cms.gov/provider-data/dataset/mj5m-pzi6</a><br></br>
                * <a href=" https://www.zip-codes.com/zip-code-api-documentation.asp"> https://www.zip-codes.com/zip-code-api-documentation.asp</a><br></br>
                </header>
                <br></br>
            </div> 
        </div>
                 
    );
}