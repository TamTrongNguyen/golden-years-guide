import unittest
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://www.goldenyearsguide.me/"
class SeleniumTests(unittest.TestCase):
    def setUp(self) -> None:
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument("--disable-gpu")
        options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(options=options)
        self.driver.implicitly_wait(10)
        self.driver.get(URL)
        return super().setUp()
    
    def tearDown(self) -> None:
        self.driver.quit()
        return super().tearDown()

    #Test that the about page has text
    def test1(self):
        self.driver.get(URL + "About")
        element = self.driver.find_element(By.XPATH, "//h1[contains(text(), 'About Golden Years Guide')]")
        
        actual = element.text
        expected = "About Golden Years Guide"

        self.assertEqual(actual, expected)

    #Test the 75254 zipcode card exists
    def test2(self):
        self.driver.get(URL + "ZipCodes")
        element = self.driver.find_element(By.XPATH, "//div[@class='card-title h5' and text()='75254']")
        
        actual = element.text
        expected = "75254"

        self.assertEqual(actual, expected)

    #Test the HIGHLANDS nursing home card exists
    def test3(self):
        self.driver.get(URL + "NursingHomes")
        element = self.driver.find_element(By.XPATH, "//div[@class='card-title h5' and text()='THE HIGHLANDS GUEST CARE CENTER LLC']")
        
        actual = element.text
        expected = "THE HIGHLANDS GUEST CARE CENTER LLC"

        self.assertEqual(actual, expected)

    #Test the HILLARY Geriatricians card exists
    def test4(self):
        self.driver.get(URL + "Geriatricians")
        element = self.driver.find_element(By.XPATH, "//div[@class='card-title h5' and text()='HILLARY GARGOTTA']")
        
        actual = element.text
        expected = "HILLARY GARGOTTA"

        self.assertEqual(actual, expected)

    # test if text exists in the about
    def test5(self):
        self.driver.get(URL + "About")
        element = self.driver.find_element(By.XPATH, "//h1[contains(text(), 'The Team Members')]")
        
        actual = element.text
        expected = "The Team Members"

        self.assertEqual(actual, expected)

    #Test postman API link
    def test6(self):
        self.driver.get(URL + "About")
        button = self.driver.find_element(By.XPATH, "/html/body/div/div/div/div/a")
        button.click()
        self.assertEqual(self.driver.current_url, "https://documenter.getpostman.com/view/30032226/2s9YRB1X3d")

    # test home title
    def test7(self):
        self.driver.get(URL)
        element = self.driver.find_element(By.XPATH, "//h1[@class='Home-title']")
        
        actual = element.text
        expected = "GOLDEN YEARS GUIDE"

        self.assertEqual(actual, expected)

    #test if text exists
    def test8(self):
        self.driver.get(URL + "ZipCodes")
        element = self.driver.find_element(By.XPATH, "//h2[contains(text(), 'Total Number of Zipcodes: 51')]")
        
        actual = element.text
        expected = "Total Number of Zipcodes: 51"

        self.assertEqual(actual, expected)

    #test if text exists
    def test9(self):
        self.driver.get(URL + "NursingHomes")
        element = self.driver.find_element(By.XPATH, "//h2[contains(text(), 'Total Number of Nursing Homes: 96')]")
        
        actual = element.text
        expected = "Total Number of Nursing Homes: 96"

        self.assertEqual(actual, expected)

    #test if text exists
    def test10(self):
        self.driver.get(URL + "Geriatricians")
        element = self.driver.find_element(By.XPATH, "//h2[contains(text(), 'Total Number of Geriatricians: 151')]")
        
        actual = element.text
        expected = "Total Number of Geriatricians: 151"

        self.assertEqual(actual, expected)
    
    #test sorting
    def test11(self):
        self.driver.get(URL + "ZipCodes")
        element = self.driver.find_element(By.XPATH, "//button[text()='Sort']")
        self.assertIsNotNone(sort_button, "The 'Sort' button exists on the page.")

    #test sorting
    def test12(self):
        self.driver.get(URL + "NursingHomes")
        element = self.driver.find_element(By.XPATH, "//button[text()='Sort']")
        self.assertIsNotNone(sort_button, "The 'Sort' button exists on the page.")

    #test sorting
    def test13(self):
        self.driver.get(URL + "Geriatricians")
        element = self.driver.find_element(By.XPATH, "//button[text()='Sort']")
        self.assertIsNotNone(sort_button, "The 'Sort' button exists on the page.")

    #test filtering
    def test14(self):
        self.driver.get(URL + "ZipCodes")
        element = self.driver.find_element(By.XPATH, "//button[text()='Select Cities']")
        self.assertIsNotNone(sort_button, "The 'Select' button exists on the page.")

    #test filtering
    def test15(self):
        self.driver.get(URL + "NursingHomes")
        element = self.driver.find_element(By.XPATH, "//button[text()='Select Cities']")
        self.assertIsNotNone(sort_button, "The 'Select' button exists on the page.")

    #test filtering
    def test16(self):
        self.driver.get(URL + "Geriatricians")
        element = self.driver.find_element(By.XPATH, "//button[text()='Select Cities']")
        self.assertIsNotNone(sort_button, "The 'Select' button exists on the page.")

    #test searching
    def test17(self):
        self.driver.get(URL + "ZipCodes")
        element = self.driver.find_element(By.XPATH, "//input")
        self.assertIsNotNone(sort_button, "The 'Search' bar exists on the page.")

    #test searching
    def test18(self):
        self.driver.get(URL + "NursingHomes")
        element = self.driver.find_element(By.XPATH, "//input")
        self.assertIsNotNone(sort_button, "The 'Search' bar exists on the page.")

    #test searching
    def test19(self):
        self.driver.get(URL + "Geriatricians")
        element = self.driver.find_element(By.XPATH, "//input")
        self.assertIsNotNone(sort_button, "The 'Search' bar exists on the page.")
    
    
    

if __name__ == "__main__":
    unittest.main()