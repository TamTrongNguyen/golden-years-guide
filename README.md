# README

## Link to Website
* [https://www.goldenyearsguide.me](https://www.goldenyearsguide.me)

## Canvas / Ed Discussion group number (please check this carefully)
* 23

## names of the team members
* Samika Iyer - Gitlab: samikaiyer1
* Meghana Inaganti - Gitlab: meghanainaganti
* Tam Nguyen - Gitlab: TamTrongNguyen
* Caleb Hu - Gitlab: bleh05
* Saarthak Mohan - Gitlab: SaarthakMohan

## name of the project (alphanumeric, no spaces, max 32 chars; this will also be your URL)
* GoldenYearsGuide

## Git SHA
* 35a79f31a44359c551da45df4da1cf96938dc000

## Phase Leaders
* Samika Iyer & Meghana Inaganti!

## Estimated vs Actual Time
* Estimated Hours: 30 hours
* Actual Hours: 40 hours

## Postman
* [https://documenter.getpostman.com/view/30032226/2s9YRB1X3d](https://documenter.getpostman.com/view/30032226/2s9YRB1X3d)

## the proposed project
* The elderly population in DFW Metroplex constitutes a significant and growing underserved community, facing a range of challenges that require specialized care and support. With the demographic shift towards an aging population, the demand for adequate healthcare services has surged, placing immense strain on resources. Many seniors struggle to access the right facilities, such as nursing homes and rehabilitation centers, due to a lack of information or guidance. In this context, a comprehensive guide to nursing homes and doctors is immensely helpful. It not only empowers elderly individuals and their families with vital information on facility options, services offered, and quality ratings but also aids in making informed decisions about the best care options available. Such a resource plays a crucial role in ensuring that our elderly citizens receive the care and attention they rightfully deserve, enhancing their quality of life and well-being in their golden years.

## URLs of at least three data sources that you will programmatically scrape using a RESTful API (be very sure about this)
* [https://www.texashealthmaps.com/lfex](https://www.texashealthmaps.com/lfex)
* [https://health.usnews.com/doctors/geriatricians](https://health.usnews.com/doctors/geriatricians)
* [https://www.hhs.texas.gov/providers/long-term-care-providers/nursing-facilities-nf](https://www.hhs.texas.gov/providers/long-term-care-providers/nursing-facilities-nf)
* [https://data.cms.gov/provider-data/dataset/4pq5-n9py](https://data.cms.gov/provider-data/dataset/4pq5-n9py)
* [https://data.cms.gov/provider-data/dataset/mj5m-pzi6](https://data.cms.gov/provider-data/dataset/mj5m-pzi6)
* [https://www.zip-codes.com/zip-code-api-documentation.asp](https://www.zip-codes.com/zip-code-api-documentation.asp)

## at least three models
* ZipCodes in DFW Metroplex
* Nursing Homes / Rehab Centers
* Geriatricians

## an estimate of the number of instances of each model
* Zipcodes in DFW Metroplex - 70
* Nursing Homes - 119
* Geriatricians - 250

## each model must have many attributes
## describe five of those attributes for each model that you can filter or sort
* ZipCodes in the DFW Metroplex
    * City
    * County
    * Median Age in the Zipcode
    * Population
    * Avg Income Per Household

* Nursing Homes
    * City
    * Number of Certified Beds
    * Overall Rating
    * HealthCare Rating
    * Resident Council, Family Council, or Both

* Geriatricians
    * City
    * Name
    * Gender
    * School
    * Graduation Year

# instances of each model must connect to instances of at least two other models
* Zipcode connects to Nursing Home and Geriatricians because each of those is in a zipcode
* Nursing Home connects to zipcode because it has a zipcode and connects to Geriatricians by sharing which geriatricians are near that home
* Geriatricians has a zipcode, connects to closest Nursing Homes

# instances of each model must be rich with different media (e.g., feeds, images, maps, text, videos, etc.) (be very sure about this)
# describe two types of media for instances of each model
* Zipcode
    * Photo of Zipcode
    * Map that displays location in DFW Metroplex
* Nursing Home
    * Picture of the home
    * Map that displays location in DFW Metroplex
* Geriatricians
    * Photo of the doctor
    * Map that displays location in DFW Metroplex

# describe three questions that your site will answer
* What nursing homes are close to my relative who is elderly, who needs care?
* Which physicians are in my area that specialize in geriatric care that I can reach out to?
* Does my zipcode have a high percentage of elderly people who could use support?
* Does the zipcode I want to move to have good resources for my elderly family?